const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

const APP_SRC = path.resolve(__dirname, "src/app");

config = {
    entry: APP_SRC,
    module: {
        rules: [
            {
                test: /\.js?/,
                include: APP_SRC,
                loader: "babel-loader",
                options: {
                    presets: ["env", "react"] 
                }
            },
            {
                test: /\.html$/,
                loader: "html-loader"
            }
        ]
    },
    devServer: {
        historyApiFallback: true
    },
    plugins: [
      new HtmlWebPackPlugin({
        template: "./src/index.html",
        filename: "./index.html"
      })
    ]
}

module.exports = config;