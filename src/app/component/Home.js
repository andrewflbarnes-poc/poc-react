import React from "react";
import PropTypes from "prop-types";

export class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            age: props.user.initialAge,
            homeLink: this.props.initialLink
        };
    }

    makeMeOlder() {
        this.setState({
            age: this.state.age += 3
        });
    }

    onChangeLink() {
        this.props.changeLink(this.state.homeLink);
    }

    onHandleChange(event) {
        this.setState({
            homeLink: event.target.value
        })
    }

    render() {
        return (
            <div>
                <div>
                    <p>Hello {this.props.user.name}, you are {this.state.age}!</p>
                    <hr/>
                    <button onClick={this.makeMeOlder.bind(this)} className="btn btn-primary">Make me older</button>
                    <hr/>
                    <button onClick={this.props.greet} className="btn btn-primary">Greet</button>
                    <hr/>
                    <input type="text" value={this.state.homeLink} onChange={this.onHandleChange.bind(this)}/>
                    <button onClick={this.onChangeLink.bind(this)} className="btn btn-primary">Change Header Link</button>
                </div>
            </div>
        )
    }
}

Home.propTypes = {
    user: PropTypes.shape({
        name: PropTypes.string.isRequired,
        initialAge: PropTypes.number.isRequired
    }),
    greet: PropTypes.func.isRequired,
    changeLink: PropTypes.func.isRequired,
    initialLink: PropTypes.string.isRequired
}

export default Home;