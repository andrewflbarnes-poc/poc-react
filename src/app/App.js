import PropTypes from 'prop-types';
import React from 'react';

import Header from './component/Header';
import Home from './component/Home';

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      homeLink: "Home",
      mountHome: true
    }
  }
  
  onGreet() {
    alert("Hello!");
  }

  onChangeLinkName(newName) {
    this.setState({
      homeLink: newName
    });
  }

  onChangeMount() {
    this.setState({
      mountHome: !this.state.mountHome
    })
  }

  render() {

    let user = {
      name: "barnesly",
      initialAge: 29
    }

    let home = "";
    if (this.state.mountHome) {
      home = <Home
        user={user}
        greet={this.onGreet}
        changeLink={this.onChangeLinkName.bind(this)}
        initialLink={this.state.homeLink}/>;
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-10 col-xs-offset-1" >
            <h1>{ this.props.name } application is alive!</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-10 col-xs-offset-1" >
            <Header homeLink={this.state.homeLink}/>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-10 col-xs-offset-1" >
            {home}
          </div>
        </div>
        <div className="row">
          <div className="col-xs-10 col-xs-offset-1" >
            <button onClick={this.onChangeMount.bind(this)} className="btn btn-primary">(Un)Mount Home Component</button>
          </div>
        </div>
      </div>
    );
  }
}

App.propTypes = {
    name: PropTypes.string.isRequired
};

export default App;